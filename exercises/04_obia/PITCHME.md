---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br><br>
@img[width=600px](assets/img/banner_conicet_conae_letrasnegras.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

### Ejercicio: Clasificación supervisada basada en objetos con datos SPOT

<br>

@fa[home text-gray fa-3x] @fa[industry text-gray fa-3x] @fa[building text-gray fa-3x]

+++

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Mejoramiento del contraste (ecualización del histograma)
- Calcular índices espectrales y texturas de GLCM
- Segmentación manual (ensayo y error)
- Segmentación con USPO
- Cómputo de las estadísticas de los segmentos
- Colecta y etiquetado de datos de entrenamiento y validación
- Clasificación supervisada por Machine Learning
- Validación
@olend
@snapend

+++

@snap[north span-100]
### Datos para el ejercicio
@snapend

@snap[east span-40]
@ul[](false)
- [SPOT 6](https://earth.esa.int/web/eoportal/satellite-missions/s/spot-6-7)
- VIS - NIR (6 m)
- PAN (1.5 m)
- Datos corregidos y fusionados
@snapend

@snap[west span-60]
<br>
@img[width=800px](assets/img/obia_region.png)
@snapend

+++

### @fa[download text-green] Datos y código para la sesión @fa[download text-green]

<br>

- Descargar el location [posgar2007_4_cba](https://www.dropbox.com/s/7x4c8fbokm60c8t/posgar2007_4_cba.zip?dl=0), y descomprimir en la carpeta *`grassdata`*
- Descargar el [código](https://gitlab.com/veroandreo/maie-procesamiento/-/raw/master/code/05_obia_code.sh?inline=false) para seguir el ejercicio

---

> @fa[tasks] **Tareas**
> 
> - Crear un mapset *`obia_spot`* en el location `posgar2007_4_cba` e importar la imagen SPOT desde la GUI forzando la resolución a 1.5m
> - Alinear la región a la extensión y resolución de alguna de las bandas importadas 
> - Mostrar la combinación RGB color natural (1: azul, 2: verde, 3: rojo, 4: NIR)
> - Hacer una ecualización de histograma para mejorar el contraste de visualización

---

### Importar datos y visualizar

@code[bash](code/05_obia_code.sh)

@[15-16](Crear mapset)
@[18-22](Importar bandas multi-espectrales)
@[24-28](Importar banda pancromática)
@[30-32](Alinear región y guardar la configuración)
@[35-43](Establecer *grey* como paleta de colores para bandas RGB)
@[45-49](Mostrar composición RGB)
@[51-55](Ecualización de colores)

+++

@img[width=800px](assets/img/spot_cba_rgb.png)

@size[22px](Composición RGB 321 color natural - SPOT 6)

---

### @fa[search] Hay valores nulos? @fa[search]

@code[bash](code/05_obia_code.sh)

@[58-64](Valores nulos en una banda)
@[66-68](Valores nulos en varias bandas)

@size[24px](@fa[exclamation-triangle text-orange] Si hubiera valores nulos, se deben rellenar antes de comenzar @fa[exclamation-triangle text-orange])

---

### Índices espectrales y texturas GLCM

@code[bash](code/05_obia_code.sh)

@[71-81](Estimar NDVI)
@[83-91](Instalar extensión *i.wi* y estimar NDWI)
@[93-94](Establecer la paleta de colores *ndwi*)
@[96-102](Estimar medidas de textura: IDM y ASM)
@[104-106](Establecer paleta *grey* para bandas de textura)

+++

@img[width=650px](assets/img/obia_frames.png)

@size[24px](Índices espectrales y texturas GLCM a partir de bandas SPOT)

+++

> Sobre qué banda calculamos las texturas?

+++

@snap[north-west span-10]
<br>
@img[width=100px](assets/img/tip.png) 
@snapend

@snap[north-east span-90 text-left]
<br>
Si no contamos con una banda pancromática, podemos crearla promediando las bandas visibles
@snapend

<br>

```bash zoom-15
# create pan-vis from RGB (if no pan available)
R=SPOT_20180621_PANSHARP.3
G=SPOT_20180621_PANSHARP.2
B=SPOT_20180621_PANSHARP.1

r.mapcalc \
  expression="PANVIS = round(($R + $G + $B) / 3)"
```
 
---

### Segmentación
#### Búsqueda de umbrales de sub y sobre-segmentación

@code[bash](code/05_obia_code.sh) 

@[130-137](Crear grupo con las bandas únicamente)
@[139-143](Definir una región más pequeña y salvarla)
@[145-154](Ejecutar una segmentación con umbral pequeño)
@[156-166](Ejecutar una segmentación con umbral más grande)

+++

@snap[west span-50 text-center]
Sobre-segmentado
![](assets/img/over_segmented.png)
@snapend

@snap[east span-50 text-center]
Sub-segmentado
![](assets/img/sub_segmented.png)
@snapend

+++

> @fa[tasks] **Tarea**
>
> Se animan a probar con otros valores y en otras regiones?

---

### Segmentación
#### Búsqueda automática de umbrales por optimización

[i.segment.uspo](https://grass.osgeo.org/grass7/manuals/addons/i.segment.uspo.html)

- Altamente intensivo para un área grande y muchas combinaciones de parámetros
    - Limitar el tamaño de la región computacional
    - Limitar el rango de los parámetros
    - Crear **superpixels** para usarlos como semillas
    - Cortar la imagen en *tiles* ([i.cutlines](https://grass.osgeo.org/grass-stable/manuals/addons/i.cutlines.html)) y paralelizar la USPO

+++

#### Generación de semillas

[i.superpixels.slic](https://grass.osgeo.org/grass-stable/manuals/addons/i.superpixels.slic.html)

- También puede utilizarse para la segmentación real
- Muy rápido para reagrupar pequeñas cantidades de píxeles similares
- Usar para reducir el número de píxeles en un factor de 4-5 y acelerar *i.segment.uspo*
- Baja compactación para mantener la separación espectral

+++

### USPO con superpixels como semillas

@code[bash](code/05_obia_code.sh) 

@[169-175](Instalar la extensión)
@[177-183](Ejecutar *i.superpixels.slic* con bajo valor de compactación)

+++

@img[](assets/img/superpixels.png)

@size[24px](RGB y resultado de la ejecución de *i.superpixels.slic*)

+++

> Cuántas semillas se generaron? Qué factor de reducción se consigue en comparación a usar todos los pixeles?

<br>
@snap[south span-70]
<br>
@img[width=90px](assets/img/tip.png) Dar una mirada a [r.info](https://grass.osgeo.org/grass-stable/manuals/r.info.html) y [g.region](https://grass.osgeo.org/grass-stable/manuals/g.region.html)
@snapend

+++

### USPO con superpixels como semillas

@code[bash](code/05_obia_code.sh) 

@[186-193](Instalar las extensiones)
@[195-205](Ejecutar la segmentación con optimización)
@[207-211](Convertir el "mejor" resultado a vector)

+++

@img[width=650px](assets/img/result_uspo.png)

@size[24px](Zoom al resultado de ejecutar la segmentación con USPO)

+++

> Cuántos segmentos obtuvieron?

<br><br>
@snap[south span-60]
<br>
@img[width=90px](assets/img/tip.png) Dar una mirada a [v.info](https://grass.osgeo.org/grass-stable/manuals/v.info.html)
<br><br>
@snapend

---

### Estadísticas: [i.segment.stats](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.stats.html)

@code[bash](code/05_obia_code.sh) 

@[214-221](Instalar las extensiones)
@[223-230](Ejecutar *i.segment.stats*)

+++

@img[width=800px](assets/img/segs_stats_attr_table.png)

@size[24px](Tabla de atributos con las estadísticas estimadas para cada objeto)

---

### Datos de entrenamiento

@code[bash](code/05_obia_code.sh)

@[233-239](Info básica de los puntos de entrenamiento provistos)
@[241-242](Copiarse el vector al mapset `obia_spot`)
@[244-248](Cuántos puntos de cada clase tenemos?)
@[250-255](Seleccionar segmentos sobre los cuales tenemos puntos de entrenamiento)
@[257-258](Cuántos segmentos contienen puntos de entrenamiento?)

+++

@img[width=750px](assets/img/points_in_segments.png)

@size[24px](Selección de segmentos con puntos de entrenamiento)

+++

### Datos de entrenamiento

@code[bash](code/05_obia_code.sh)

@[260-262](Agregar columna al vector con los segmentos para luego transferir la clase)
@[264-269](Asignar la clase de los puntos a los segmentos)
@[271-275](Cuántos segmentos de cada clase tenemos?)

+++

@snap[north]
#### Datos de entrenamiento
@snapend

@snap[west span-50]
<br>
![](assets/img/assign_color_to_train_segments.png)
@snapend

@snap[east span-45 text-08 text-left]
Asignación de colores interactivamente
<br><br>
@ol[](false)
- Ir agregando valores
- Seleccionar colores 
- Previsualizar
- Guardar la paleta creada como *obia_urban* para reusar posteriormente
@olend
@snapend

+++

@snap[north-west span-20]
@img[width=120px](assets/img/tip.png)
@snapend

@snap[north span-80]
<br>
Selección y etiquetado de datos de entrenamiento y validación
@snapend

@snap[west span-100]
<br><br>
@ul[](false)
- Ejecutar una clasificación no supervisada con 10 clases
- Extraer una *x* cantidad de puntos por clase ([r.sample.category](https://grass.osgeo.org/grass-stable/manuals/addons/r.sample.category.html))
- Etiquetar los puntos manualmente
- Usar puntos para transferir las etiquetas a los segmentos como ya vimos
@snapend

+++

```
# Unsupervised classification
i.group group=spot_all \
  input=SPOT_20180621_ASM,SPOT_20180621_IDM,SPOT_20180621_NDVI,SPOT_20180621_NDWI,SPOT_20180621_PAN,SPOT_20180621_PANSHARP.1,SPOT_20180621_PANSHARP.2,SPOT_20180621_PANSHARP.3,SPOT_20180621_PANSHARP.4
i.cluster group=spot_all signaturefile=sig classes=10
i.maxlik group=spot_all signaturefile=sig output=uns_clas

# install extension
g.extension r.sample.category

# get n points per class
r.sample.category input=uns_clas \
  output=uns_clas_points \
  npoints=150

# Manually label points


```
---

### Clasificación con Machine learning

@code[bash](code/05_obia_code.sh)

@[278-284](Instalar la extensión)
@[286-304](Ejecutar la clasificación)
@[306-309](Establecer paleta de colores)

+++

@img[width=700px](assets/img/obia_result.png)

@size[24px](Resultado de la clasificación supervisada con Machine Learning basada en objetos)

+++

> @fa[exclamation-triangle text-orange fa-2x] 
> 
> El proceso de clasificación usualmente conlleva una serie de iteraciones que implican selección de variables más importantes, búsqueda de más/mejores datos de entrenamiento y validación

---

### Validación

- Se usan datos independientes para validar las clasificaciones
- Se construye una **matriz de confusión** que permite visualizar los errores por clase en los elementos que están fuera de la diagonal
- Se estiman varias medidas relacionadas a la precisión, ej.: **overall accuracy** y **kappa**

+++

### Validación

Distintas opciones:
1. Generar un nuevo set de puntos y etiquetarlos
2. Separar el set de puntos etiquetados en *train* y *test* de antemano

+++

#### Validación en GRASS GIS

[r.kappa](https://grass.osgeo.org/grass-stable/manuals/r.kappa.html)

- Necesita mapas raster como *input*
  - Transformar los segmentos de validación a formato raster usando la columna *`class`* como fuente de valores para los pixeles

+++

> @fa[tasks] Tarea 
>
> Generar un set de validación de al menos 50 segmentos y ejecutar [**r.kappa**](https://grass.osgeo.org/grass-stable/manuals/r.kappa.html)

+++

#### Validación en GRASS GIS

@code[bash](code/05_obia_code.sh)

@[312-321](Una vez creado el vector de segmentos con etiquetas *testing*, convertirlo a formato raster)
@[323-326](Ejecutar *r.kappa*)

+++

Alternativamente, podemos separar el set de puntos etiquetados en *train* y *test*
<br><br><br>
Vamos a @fab[r-project text-blue fa-3x]

+++

@code[r zoom-15](code/05_obia_code.sh)

@[334-338](Cargar librerías)
@[340-342](Leer el vector desde GRASS)
@[344-349](Crear set de validación)
@[351-352](Separar set de entrenamiento)
@[354-356](Escribir los vectores a GRASS nuevamente)

+++

> @fa[tasks] **Tarea**
>
> Ejecutar nuevamente la clasificación usando sólo el vector *train*

+++

@code[bash](code/05_obia_code.sh)

@[367-374](Agregar columna al vector *test*)
@[376-379](Obtener las clases predichas para los segmentos de validación)

+++

#### Validación en R

@code[r](code/05_obia_code.sh)

@[382-388](Leer el vector test que tiene la clase predicha)
@[390-394](Cargar la librería *caret* y obtener la matriz de confusión)

+++

> @fa[tasks] **Tarea**
>
> - Explorar el módulo [v.kcv](https://grass.osgeo.org/grass-stable/manuals/v.kcv.html)
> - Cómo se podría haber utilizado para separar los puntos etiquetados en training y test?
> - Cuál es la diferencia entre dicho módulo y la separación que realizamos en R?

<br><br><br>
@snap[south span-100]
<br>
@img[width=90px](assets/img/tip.png) Dar una mirada a [v.divide.training_validation](https://github.com/mundialis/v.divide.training_validation)
<br><br>
@snapend

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br>
Nos vemos en algunos días con:
<br><br>
@fa[layer-group text-green] [Series de tiempo en GRASS GIS](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/06_time_series&grs=gitlab#/) @fa[layer-group text-green]
<br>

<br><br>
Opcionalmente:
<br><br>
[Procesamiento de datos satelitales en GRASS GIS](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/04_imagery&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
